var path = require('path')
var express = require('express')
var app = express()

app.use('/demo', (req, res, next) => {
  var baseName = path.basename(req.originalUrl)
  var timeout = null;
  switch (baseName) {
    case 'script_first.js':
      timeout = 1000;
      break;
    case 'script_second.js':
      timeout = 4000;
      break;
  }
  if(!timeout) {
    next()
  }
  else {
    setTimeout(next, timeout);
  }
})
app.use('/demo', express.static('./front/demo'))
app.get('/*', express.static('./front'))

app.listen(3000)